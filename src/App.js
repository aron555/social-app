import React from 'react';
import SidebarMenu from './Components/SidebarMenu/SidebarMenu';
import Chats from './Components/Chats/Chats';
import Groups from './Components/Groups/Groups';
import Status from './Components/Status/Status';
import Calls from './Components/Calls/Calls';
import Settings from './Components/Settings/Settings';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import MessageIcon from '@material-ui/icons/esm/Message';
import GroupIcon from '@material-ui/icons/esm/Group';
import LibraryBooksIcon from '@material-ui/icons/esm/LibraryBooks';
import CallIcon from '@material-ui/icons/esm/Call';
import SettingsIcon from '@material-ui/icons/esm/Settings';

export default function App(props) {
  const routes = [
    {
      name: 'Чаты',
      icon: <MessageIcon />,
      path: '/',
      exact: true,
      component: () => (
        <Chats
          users={props.state.users}
          messages={props.state.messages}
          chats={props.state.chats}
          onlineUsers={props.state.onlineUsers}
          recentChats={props.state.recentChats}
          addPost={props.addPost}
        />
      ),
    },
    {
      name: 'Группы',
      icon: <GroupIcon />,
      path: '/groups',
      component: () => <Groups users={props.state.users} messages={props.state.messages} chats={props.state.chats} />,
    },
    {
      name: 'Статус',
      icon: <LibraryBooksIcon />,
      path: '/status',
      component: () => <Status />,
    },
    {
      name: 'Звонки',
      icon: <CallIcon />,
      path: '/audio-call',
      component: () => <Calls />,
    },
    {
      name: 'Настройки',
      icon: <SettingsIcon />,
      path: '/settings',
      component: () => <Settings />,
    },
  ];

  return (
    <Router>
      <div className="App">
        <div className="main-wrapper">
          <div className="content main_content">
            <SidebarMenu routes={routes} />

            <Switch>
              {routes.map((route, index) => (
                <Route key={index} path={route.path} exact={route.exact} children={<route.component />} />
              ))}
            </Switch>
          </div>
        </div>
      </div>
    </Router>
  );
}
