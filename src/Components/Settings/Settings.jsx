import React from 'react';
import SettingsLeftSidebar from './SettingsLeftSidebar/SettingsLeftSidebar';
import SettingsMain from './SettingsMain/SettingsMain';

export default function Settings() {
  return (
    <React.Fragment>
      <SettingsLeftSidebar />
      <SettingsMain />
    </React.Fragment>
  );
}
