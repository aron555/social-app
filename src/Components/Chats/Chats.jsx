import React from 'react';
import ChatSidebar from './ChatSidebar/ChatSidebar';
import Middle from './Middle/Middle';
import RightSidebar from './RightSidebar/RightSidebar';

export default function Chats(props) {
  return (
    <React.Fragment>
      <ChatSidebar onlineUsers={props.onlineUsers} recentChats={props.recentChats} />
      <Middle users={props.users} messages={props.messages} chats={props.chats} addPost={props.addPost} />
      <RightSidebar />
    </React.Fragment>
  );
}
