import React from 'react';
import GroupsSidebar from './GroupsSidebar/GroupsSidebar';
import GroupMiddle from './GroupMiddle/GroupMiddle';

export default function Groups(props) {
  return (
    <React.Fragment>
      <GroupsSidebar />
      <GroupMiddle users={props.users} messages={props.messages} chats={props.chats} />
    </React.Fragment>
  );
}
